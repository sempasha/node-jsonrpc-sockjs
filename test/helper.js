/**
 * @fileOverview Полезные функции для проведения тестов
 * @ignore
 */

var http = require('http'),
    sockjs = require('sockjs'),
    client = require('sockjs-client'),
    main = require('../'),
    server_prefix = '/service',
    server_port = 9999;

exports.createServer = function (done) {
    var server = {
        sockjs: sockjs.createServer(),
        http: http.createServer(),
        remote_clients: [],
        local_clients: []
    };

    server.sockjs.installHandlers(server.http, {
        prefix: server_prefix,
        log: function (severity, line) {
            if (severity === 'error') {
                console.error(line);
            }
        }
    });

    server.sockjs.on('connection', function (connection) {
        server.local_clients.push(connection);
        connection.once('close', function () {
            server.local_clients.splice(server.local_clients.indexOf(connection), 1);
        });
    });

    server.http.listen(server_port, '0.0.0.0', function () {
        if (done) {
            done();
        }
    });

    return server;
};

exports.destroyServer = function (server, done) {
    server.local_clients.forEach(function (client) {
        client.close();
    });
    server.remote_clients.forEach(function (client) {
        client.close();
    });
    server.http.close(function () {
        if (done) {
            done();
        }
    });
};

exports.createRemoteClient = function (server) {
    var remote_client = client.create('http://localhost:' + server_port + server_prefix);
    remote_client.on('error', function (error) {
        console.error(error);
    });
    remote_client.once('connection', function () {
    });
    remote_client.once('close', function () {
        server.remote_clients.splice(server.remote_clients.indexOf(remote_client), 1);
    });
    server.remote_clients.push(remote_client);
    return remote_client;
};

exports.createClientsPair = function (server, options, done) {
    if (typeof options === 'function') {
        done = options;
        options = {};
    }
    var remote_client = exports.createRemoteClient(server);
    server.sockjs.once('connection', function (connection) {
        if (typeof options === 'object') {
            if (typeof options.socket === 'undefined') {
                options.socket = connection;
            }
        }
        done(remote_client, new main.Peer(options));
    });
};

exports.addClientProcedure = function (client, name, procedure) {
    if (client._getRequestedProcedure === main.Peer.prototype._getRequestedProcedure) {
        client._controller = {};
        client._getRequestedProcedure = function (name) {
            if (name in this._controller) {
                return {
                    'function': this._controller[name],
                    context: this
                };
            }
        };
    }
    client._controller[name] = procedure;
};