/**
 * @fileOverview Тесты класса [main.Peer]{@link module:main.Peer}
 * @ignore
 */

/* global describe, it, before, after */
var assert = require('assert'),
    helper = require('./helper'),
    main = require('../');

describe('main.Peer', function() {
    var server;

    before(function (done) {
        server = helper.createServer(done);
    });

    after(function (done) {
        helper.destroyServer(server, done);
    });

    describe('#constructor', function () {
        var create = function (options) {
            return function () {
                return new main.Peer(options);
            };
        };

        it('Обязательным папраметром является options.socket (net.Socket)',
            function (done) {
                var remote = helper.createRemoteClient(server);

                server.sockjs.once('connection', function (connection) {
                    assert.throws(create(), 'Параметр options должен быть определён');
                    assert.throws(create(null), 'Параметр options должен быть объектом, не null');
                    assert.throws(create(''), 'Параметр options должен быть объектом, не строкой');
                    assert.throws(create(0), 'Параметр options должен быть объектом, не числом');
                    assert.throws(create({}), 'Параметр options должен быть объектом, как минимум с одним полем .socket');
                    assert.throws(create({socket: null}), 'Параметр options.socket должен быть объектом net.Socket, ' +
                        'не null');
                    assert.throws(create({socket: ''}), 'Параметр options.socket должен быть объектом net.Socket, ' +
                        'не строкой');
                    assert.throws(create({socket: 0}), 'Параметр options.socket должен быть объектом net.Socket, ' +
                        'не числом');
                    assert.doesNotThrow(create({socket: connection}), 'Ошибка при верном параметре options.socket');
                    remote.once('close', function () {
                        done();
                    });
                    remote.close();
                });
            });

        it('Сокет для которго создается пир должен быть доступным для чтения и записи',
            function (done) {
                var remote = helper.createRemoteClient(server);

                server.sockjs.once('connection', function (connection) {
                    connection.once('close', function () {
                        assert.throws(create({socket: connection}), 'Соединение уже закрыто');
                        done();
                    });
                    remote.close();
                });
            });
    });

    describe('#send', function () {
        it('Производит отправку сообщений из буфера Peer#_sendBuffer в порядке очереди',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var count = 10,
                        accepted = [],
                        i;

                    remote.on('data', function (data) {
                        try {
                            accepted.push(main.parseMessage(data));
                            if (accepted.length === count) {
                                accepted.forEach(function (request, index) {
                                    assert.strictEqual(request.id, 'id:' + index, 'Неверный порядок отправки ' +
                                        'сообщений, ожидается запрос с id="id:' + index + '", а получен с ' +
                                        'id="' + request.id + '"');
                                });
                                remote.once('close', function () {
                                    done();
                                });
                                remote.close();
                            }
                        } catch (error) {
                            done(error);
                        }
                    });

                    for (i = 0; i < count; i++) {
                        local._sendBuffer.push(new main.Request('id:' + i, 'ping'));
                    }
                    local.send();
                });
            });

        it('После отправки сообщения, оно удаляется из очереди',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    local._sendBuffer.push(new main.Request(main.createIdentifier(), 'ping'));
                    local.send();
                    assert.strictEqual(local._sendBuffer.length, 0, 'Сообщение не удалено из буфера');
                    remote.once('close', function () {
                        done();
                    });
                    remote.close();
                });
            });
    });

    describe('#isConnected', function () {
        it('Проверяет статус подключения клиента, если клиент подключен, возвращает true, в противном случае false',
            function (done) {
                function disconnect_from_server (done) {
                    helper.createClientsPair(server, function (remote, local) {
                        local.on('disconnect', function () {
                            assert.ok(!local.isConnected(), 'Соединение не может быть рабочим после получения ' +
                                'события disconnect');
                        });
                        remote.once('close', function () {
                            assert.ok(!local.isConnected(), 'Соединение не может быть рабочим, если даже клиент в ' +
                                'курсе, что его отключили');
                            done();
                        });

                        assert.ok(local.isConnected(), 'Соединение не может быть не рабочим на момент ' +
                            'создания пира');
                        local._socket.close();
                        assert.ok(!local.isConnected(), 'Соединение не может быть рабочим после отключения сокета');
                    });
                }

                function disconnect_from_client (done) {
                    helper.createClientsPair(server, function (remote, local) {
                        local.on('disconnect', function () {
                            assert.ok(!local.isConnected(), 'Соединение не может быть рабочим после получения ' +
                                'события disconnect');
                            done();
                        });
                        assert.ok(local.isConnected(), 'Соединение не может быть не рабочим на момент ' +
                            'создания пира');

                        remote.close();
                    });
                }

                disconnect_from_server(function () {
                    disconnect_from_client(done);
                });
            });
    });

    describe('#disconnect', function () {
        it('Осуществляет закрытие соединения с клиентом',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    remote.once('close', function () {
                        done();
                    });
                    local.disconnect();
                });
            });

        it('После отключения генерируется событие disconnect',
            function (done) {
                this.timeout(server.sockjs.options.disconnect_delay + 1000);
                helper.createClientsPair(server, function (remote, local) {
                    var remote_closed = false,
                        local_closed = false,
                        timer;

                    local.once('disconnect', function () {
                        local_closed = true;
                        if (timer) {
                            clearTimeout(timer);
                            timer = null;
                        }
                        if (remote_closed) {
                            done();
                        }
                    });
                    remote.once('close', function () {
                        remote_closed = true;
                        timer = setTimeout(function () {
                            if (!local_closed) {
                                done(new Error('Соединение было закрыто, но события disconnect не произошло'));
                            }
                        }, server.sockjs.options.disconnect_delay);
                    });
                    local.disconnect();
                });
            });
    });
});