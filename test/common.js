/**
 * @fileOverview Набор функциональных тестов для модуля
 * @ignore
 */

/* global describe, it, before, after */
var assert = require('assert'),
    helper = require('./helper'),
    main = require('../');

describe('Функциональные тесты', function () {
    var server;

    before(function (done) {
        server = helper.createServer(done);
    });

    after(function (done) {
        helper.destroyServer(server, done);
    });

    describe('Отправка запросов', function () {
        it('Отправляет запрос вызовом main.Peer#request',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var request = local.request('ping', {payload: 'hello'});

                    remote.once('data', function (data) {
                        assert.equal(request.toString(), data, 'Отправлен неверный запрос');
                        remote.once('close', done);
                        remote.close();
                    });
                });
            });

        it('Принимает результат выполнения запроса',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var request = local.request('ping', {payload: 'hello'});

                    remote.once('data', function (data) {
                        var response = new main.Response(request.id, null, request.params.payload);
                        remote.write(response.toString());
                    });

                    request.onFinish(function (error, result) {
                        assert.equal(request.params.payload, result, 'Получен неверный ответ');
                        remote.once('close', done);
                        remote.close();
                    });
                });
            });

        it('Принимает ошибки выполнения запроса',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var request = local.request('ping', {payload: 'hello'});

                    remote.once('data', function (data) {
                        var error = new main.Error(10, 'Error message'),
                            response = new main.Response(request.id, error);

                        remote.write(response.toString());
                    });

                    request.onFinish(function (error, result) {
                        assert.ok(error instanceof main.Error, 'Ошибка не получена');
                        remote.once('close', done);
                        remote.close();
                    });
                });
            });

        it('Отслеживает наступления таймаута ожидания ответа на запрос, ошибка "Request timeout" (код -32001)',
            function (done) {
                var timeout = 0.01;
                helper.createClientsPair(server, {request_timeout: timeout}, function (remote, local) {
                    var request = local.request('ping', {payload: 'hello'}),
                        timeout_exceeded = false;

                    request.onFinish(function (error, result) {
                        assert.ok(error instanceof main.Error, 'Ошибки не произошло');
                        assert.strictEqual(error.code, -32001, 'Код ошибки должен быть -32001');
                        assert.strictEqual(error.message,
                            'Request timeout', 'Сообщение ошибки должен быть "Request timeout"');
                        timeout_exceeded = true;
                    });

                    setTimeout(function () {
                        assert.ok(timeout_exceeded, 'Таймаут ожидания ответа не произошёл за отведённое время - ' +
                            (timeout * 1000) + 'мс');
                        done();
                    }, timeout * 1000 + 1);
                });
            });
    });

    describe('Отправка уведомлений', function () {
        it('Отправляет запрос вызовом main.Peer#notify',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var notification = local.notify('ping', {payload: 'hello'});

                    remote.once('data', function (data) {
                        assert.equal(notification.toString(), data, 'Отправлен неверный запрос');
                        remote.once('close', done);
                        remote.close();
                    });
                });
            });

        it('Считается, что уведомление завершилось удачно, если отправка осстоялась',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var notification = local.notify('ping', {payload: 'hello'});

                    notification.onFinish(function (error, result) {
                        assert.strictEqual(error, null, 'Ошибки не должно быть в случае удачной отправки сообщения');
                        assert.strictEqual(result, null, 'Результат уведомления всегда null, если нет ошибки');
                        remote.once('close', done);
                        remote.close();
                    });
                });
            });

        it('Отслеживает наступления таймаута ожидания отправки уведомления, ошибка "Request timeout" (код -32001)',
            function (done) {
                var timeout = 0.01;
                helper.createClientsPair(server, {request_timeout: timeout}, function (remote, local) {
                    local.send = function () {
                        delete local.send;
                    };

                    var notification = local.notify('ping', {payload: 'hello'}),
                        timeout_exceeded = false;

                    notification.onFinish(function (error, result) {
                        assert.ok(error instanceof main.Error, 'Ошибки не произошло');
                        assert.strictEqual(error.code, -32001, 'Код ошибки должен быть -32001');
                        assert.strictEqual(error.message,
                            'Request timeout', 'Сообщение ошибки должен быть "Request timeout"');
                        timeout_exceeded = true;
                    });

                    setTimeout(function () {
                        assert.ok(timeout_exceeded, 'Таймаут отправки уведомления не произошёл за отведённое время - ' +
                            (timeout * 1000) + 'мс');
                        done();
                    }, timeout * 1000 + 1);
                });
            });
    });

    describe('Приём запросов', function () {
        it('Принимает входящие запросы',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var request = main.Request(main.createIdentifier(), 'ping', {payload: 'hello'}),
                        response = main.Response(request.id, null, request.params.payload);

                    helper.addClientProcedure(local, 'ping', function (payload) {
                        return payload;
                    });

                    remote.write(request.toString());
                    remote.once('data', function (data) {
                        assert.equal(response.toString(), data, 'Клиенту отправлен неверный ответ');
                        remote.once('close', done);
                        remote.close();
                    });
                });
            });

        it('В случае, если метод не найден, возвращает ошибку "Method not found" (код -32601)',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var request = main.Request(main.createIdentifier(), 'ping', {payload: 'hello'}),
                        error = main.Error(-32601, "Method not found"),
                        response = main.Response(request.id, error);

                    helper.addClientProcedure(local, 'pong', function (payload) {
                        return payload;
                    });

                    remote.write(request.toString());
                    remote.once('data', function (data) {
                        assert.equal(response.toString(), data, 'Клиенту отправлен неверный ответ');
                        remote.once('close', done);
                        remote.close();
                    });
                });
            });
    });

    describe('Приём уведомлений', function () {
        it('Принимает входящие уведомления',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var request = main.Request(null, 'ping', {payload: 'hello'});

                    helper.addClientProcedure(local, 'ping', function (payload) {
                        remote.once('close', done);
                        remote.close();
                        return payload;
                    });

                    remote.write(request.toString());
                });
            });

        it('Не отвечает на уведомления в случае ошибки выполнения запроса (либо в случае, таймаута)',
            function (done) {
                helper.createClientsPair(server, function (remote, local) {
                    var request = main.Request(null, 'ping', {payload: 'hello'}),
                        timer;

                    helper.addClientProcedure(local, 'ping', function (payload) {
                        timer = setTimeout(function () {
                            done();
                        }, 100);
                        throw new Error('Error message');
                    });

                    remote.write(request.toString());
                    remote.once('data', function () {
                        if (timer) {
                            clearTimeout(timer);
                        }
                        done(new Error('Отправки ошибки о выполнении процедуры в рамказ уведомления быть не должно'));
                    });
                });
            });
    });
});