/**
 * @fileOverview Расширение класса [jsonrpc.Peer]{@link external:jsonrpc.Peer}, для обслуживания SockJS подключения
 * клиент JSON-RPC сервиса
 * @ignore
 */

/**
 * Событие говорит о том, что клиент утратил соединение с сервисом, это может произойти в результате вызова метода
 * [Peer#disconnect]{@link module:main.Peer#disconnect}, либо в результате обрыва соединения по
 * любой другой причине. После того, как соединение было утеряно пир более не может продолжать деятельность (ни
 * принимать входящие сообщения, ни отправлять сообщения удалённому клиенту), так что все запросы (входящие и исходящие)
 * будут завершенены с ошибкой <code>"Connection closed"</code> (код <code>-32002</code>).
 *
 * @event module:main.Peer#disconnect
 * @type {Undefined}
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var Peer = require('node-jsonrpc/lib/peer');

/**
 * @ignore
 */
var main = require('../');

/**
 * Список параметров клиента по умолчанию
 *
 * @alias module:main.Peer~Options
 * @typedef
 * @type {Object}
 * @property {?Number} [request_timeout=60] Таймаут выполнения запроса (сек.), если после отправки запроса на
 * выполнение процедуры, от удалённой стороны не поступило ответа в течение этого времени, то мы считаем,
 * что запрос провалился с ошибкой <code>"Request timeout"</code> (код <code>-32001</code>).
 * @property {net.Socket} socket активное [SockJS]{@link https://github.com/sockjs/sockjs-node} соединение,
 * интерфейс соединения сообвествует [net.Socket]{@link http://nodejs.org/api/net.html#net_class_net_socket}
 */
var Options = {
    request_timeout: 60,
    socket: null
};

/**
 * Расширение класса [jsonrpc.Peer]{@link external:jsonrpc.Peer}, для обслуживания SockJS подключения
 * клиент JSON-RPC сервиса. Помимо опций принимаемых
 *
 * @alias module:main.Peer
 * @abstract
 * @constructor
 * @augments external:jsonrpc.Peer
 * @param {module:main.Peer~Options} options
 * @fires module:main.Peer#connect
 */
module.exports = exports = function (options) {
    if (!(this instanceof exports)) {
        return new exports(options);
    }
    if (typeof options !== 'object' || options === null) {
        throw new TypeError('options must be a non-null object');
    }
    if (typeof options.request_timeout === 'undefined') {
        options.request_timeout = Options.request_timeout;
    }
    if (typeof options.socket !== 'object' || options.socket === null) {
        throw new TypeError('options.socket must be an instance of readable/writable socket object (like net.Socket)');
    }
    if (typeof options.socket.writable === 'undefined' || !options.socket.writable) {
        throw new TypeError('options.socket must be an instance of readable/writable socket, ' +
            'non-writable socket given');
    }
    if (typeof options.socket.readable === 'undefined' || !options.socket.readable) {
        throw new TypeError('options.socket must be an instance of readable/writable socket, ' +
            'non-readable socket given');
    }
    Peer.call(this, options);
    var self = this;
    self._socket = options.socket;
    self._socket.on('data', function (data) {
        self._acceptMessage(data);
    });
    self._socket.on('close', function () {
        var id, index;

        // void all incoming messages which has been accepted by closed SockJS connection
        for (id in self._incomingRequests) {
            self._incomingRequests[id].finish(new main.Error(-32002, "Connection closed"));
        }
        // void all outgoing messages which has been sent by closed SockJS connection
        for (id in self._outgoingRequests) {
            index = self._sendBuffer.indexOf(self._outgoingRequests[id]);
            if (index === -1) {
                self._outgoingRequests[id].finish(new main.Error(-32002, "Connection closed"));
            }
        }
        self._socket = null;
        self.emit('disconnect');
    });
};

inherits(exports, Peer);

/**
 * Переобъявляем метод [jsonrpc.Peer#send]{@link external:jsonrpc.Peer#send}
 *
 * @alias module:main.Peer#send
 * @override external:jsonrpc.Peer#send
 * @returns {Undefined}
 */
exports.prototype.send = function () {
    if (this._socket !== null && this._socket.writable) {
        while(this._sendBuffer.length > 0) {
            var message = this._sendBuffer.shift();
            this._socket.write(message.toString());
            if (message instanceof main.Request && message.id === null) {
                message.finish();
            }
        }
    }
};

/**
 * Проверяет статус соединения, если соединение установлено, то возвращает <code>true</code>, <code>false</code>
 * в противном случае
 *
 * @alias module:main.Peer#isConnected
 * @returns {Boolean}
 */
exports.prototype.isConnected = function () {
    return this._socket !== null && this._socket.readable && this._socket.writable;
};

/**
 * Добавляем метод отключения удалённого клиента от сервера
 *
 * @alias module:main.Peer#disconnect
 * @returns {Undefined}
 * @fires module:main.Peer#disconnect
 */
exports.prototype.disconnect = function () {
    if (this._socket !== null) {
        this._socket.close(1000, 'NORMAL_CLOSURE');
    }
};