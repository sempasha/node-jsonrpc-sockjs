/**
 * @external jsonrpc
 * @desc Реализация клиент-серверного взаимодействия по протоколу
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification} для JavaScript.
 * Не имеет реализации транспорта сообщений, для реализации отправки/получения JSON-RPC сообщений заложен интерфейс и
 * асбтрактные методы.
 * @see [node-jsonrpc]{@link https://bitbucket.org/sempasha/node-jsonrpc}
 */

/**
 * @class external:jsonrpc.Peer
 * @desc Абстрактный класс описывающий точку являющуюся субъектом/объектом JSON-RPC сообщения,
 * т.е. выполняющую роль либо клиента, вызывающего удалённые процедуры на сервере, либо самого сервера, на котором
 * происходит выполнение вызванных удалённых процедур. В данном классе отсутствует реализация транспорта сообщений.
 * @see [jsonrpc.Peer]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Peer}
 */

/**
 * @method external:jsonrpc.Peer.send
 * @desc Метод производит отправку JSON-RPC сообщений из буффера. Перед отправкой запроса необходимо удостовериться, что
 * запрос не был отменён (см. [jsonrpc.Request#isFinished]{@link external:jsonrpc.Request#isFinished}). Если сообщение -
 * это запрос типа [уведомление]{@link http://www.jsonrpc.org/specification#notification}, то после того,
 * как сообщение отправлено для запроса будет зафиксирован положительный результат выполнения
 * (см. [jsonrpc.Request#finish]{@link external:jsonrpc.Request#finish})
 * @see [jsonrpc.Peer#send]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Peer%23send}
 */

/**
 * @fileOverview Расширение [node-jsonrpc]{@link external:jsonrpc} для обслуживания SockJS подключения клиента JSON-RPC
 * сервиса
 * @module main
 * @augments external:jsonrpc
 */

/**
 * Расширяем модуль [node-jsonrpc]{@link external:jsonrpc}
 *
 * @type {exports}
 * @ignore
 */
module.exports = exports = require('node-jsonrpc');

/**
 * Расширение [jsonrpc.Peer]{@link external:jsonrpc.Peer} для обслуживания SockJS подключения клиента JSON-RPC сервиса
 *
 * @ignore
 * @alias module:main.Peer
 * @class
 */
exports.Peer = require('./lib/peer');