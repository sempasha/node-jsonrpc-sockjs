Дескриптор клиента JSON-RPC сервиса, подключенного по SockJS
---

# О проекте

Расширение [node-jsonrpc](https://bitbucket.org/sempasha/node-jsonrpc) для обслуживания SockJS подключения клиента
JSON-RPC сервиса. Расширение представляет абстрактную реализацию дескриптора клиента со стороны сервиса.
Абстрактность заключается в отсутствии контроллера (для того, чтобы удалённый клиент мог вызывать процедуры на сервисе
необходима реализация контроллера процедур), но контроллер можно подключить если к данному модулю добавить
имплеменнтацию метода `jsonrpc.Peer#_getRequestedProcedure`. Данный метод как раз будет возвращать запрошеный метод
контроллера (функцию-процедуры) с заданным контекстом исполнения. Проект основан на библиотеке
[`node-jsonrpc`](https://bitbucket.org/sempasha/node-jsonrpc)

# Установка

Библиотека доступна на сервисе [Bitbucket](https://bitbucket.org/sempasha/node-jsonrpc-sockjs). Есть 3 варианта её
загрузки/установки:

 1. Через менеджер пакетов [NPM](#NPM);
 1. Через [Git](#Git);
 1. Загрузка [Zip архива](Zip).

В репозитории для каждой минорной версии библиотеки есть ветка с названием соответствующим этой версии. Название ветки
задаётся в формате `v{major}.{minor}`, где `{major}` - соответствует номеру мажорной версии библиотеки, а `{minor}` -
минорной. (библиотека поддерживает [симантичесую систему определения версии](http://semver.org/)). Так же в главной
ветке `master` репозитория всегда находится самая свежая версия библиотеки. Примеры:

 1. Ветка `v0.1` - содержит библиотеку версии `0.1.x`, где `x` - макисальная версия патча среди пакета версий 0.1.*;
 1. Ветка `v0.5` - содержит библиотеку версии `0.5.x`, `x` - аналогично пункту 1;
 1. `master` - содержит библиотеку самой последней версии.

## NPM

Установка [NPM](https://www.npmjs.org/) пакета последней версии с
[Bitbucket](https://bitbucket.org/sempasha/node-jsonrpc-sockjs):

```
npm install git+https://bitbucket.org/sempasha/node-jsonrpc-sockjs.git --save
```

В этом случае произойдёт установка последней актуальной версии пакета. Для того, чтобы установить пакет определённой
версии - укажите соответствую ветку в адресе:

```
npm install git+https://bitbucket.org/sempasha/node-jsonrpc-sockjs.git#v{major}.{minor} --save
```

В качестве альтернативы можно использовать установку из [tarball](http://en.wikipedia.org/wiki/Tarball):

```
$ npm install https://bitbucket.org/sempasha/node-jsonrpc-sockjs/get/master.tar.gz --save
```

или

```
$ npm install https://bitbucket.org/sempasha/node-jsonrpc-sockjs/get/v{minor}.{major}.tar.gz --save
```

## Git

```
git clone git@bitbucket.org:sempasha/node-jsonrpc-sockjs.git
```

## Zip

Для загрузки последней версии загрузить [zip мастер ветки](https://bitbucket.org/sempasha/node-jsonrpc-sockjs/get/master.zip):

```
https://bitbucket.org/sempasha/node-jsonrpc-sockjs/get/master.zip
```

Для загрузки специфической версии:

```
https://bitbucket.org/sempasha/node-jsonrpc-sockjs/get/v{minor}.{major}.zip
```

# Тесты

Все компоненты библиотеки покрыты тестами. Запуск тестов:

```
npm run test
```

# API

Документация по API собирается с помощью [JSDoc](http://usejsdoc.org/), для сборки можно использовать NPM run-script

```
npm run docs
```

По умолчанию после сборки документация доступна в папке проекта - [docs](docs/index.html).

Библиотека не поддерживает возможность совершения [Batch](http://www.jsonrpc.org/specification#batch) запросов.